/**
 * <pre>
 * API Type: Angular Controller,
 * Module: bookModule,
 * Description :
 * This is API used as controller for controlling the book.html view.
 * </pre>
 */
(function () {
    /* Start: Controller Configuration */
    var moduleName = "bookModule";
    var module = angular.module(moduleName);
    var controllerName = "BookController";
    var injectedDependencies = ['$scope', '$stateParams','bookService','$state'];
    /* End: Controller Configuration */

    /* Start:Controller Definition */
    var controller = function ($scope, $stateParams,bookService,$state) {

        var setBook = function (book) {
            if(book) {
                $scope.book = book;
            } else {
                /*If book is not found then redirect to book list*/
                $state.go('books');
            }
        };
        //Get Book
        bookService.getBookById($stateParams.id,setBook);

    };
    /* End:Controller Definition */

    /* Register the controller */
    controller.$inject = injectedDependencies;
    module.controller(controllerName, controller);
}());
