/**
 * <pre>
 * API Type: Angular Module,
 * Description :
 * This is an angular Module used bootstrap the application by angular.
 * It has associated controller and service which deals with data management on the views
 * </pre>
 */
(function () {
    /* Start:Module Configuration */
    var moduleName = "bookModule";
    // Module Dependencies
    var injectedDependencies = ['ui.router'];
    /* End Configuration */
    var configFunction = function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state('books', {
            url: '/',
            views: {
                'booksView': {
                    templateUrl: 'books.html',
                    controller: 'BooksController'
                }
            }

        }).state('books.book', {
            url: 'book/:id',
            views: {
                'bookView': {
                    templateUrl: 'book.html',
                    controller: 'BookController'
                }
            }
        });
        $urlRouterProvider.otherwise('/');
    }

    var config = ['$stateProvider', '$urlRouterProvider', configFunction];
    // Register Module
    angular.module(moduleName, injectedDependencies).config(config);
}());
