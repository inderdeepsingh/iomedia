/**
 * <pre>
 * API Type: Angular Controller,
 * Module: bookModule,
 * Description :
 * This is API used as controller for controlling the books.html view.
 * </pre>
 */
(function () {
    /* Start: Controller Configuration */
    var moduleName = "bookModule";
    var module = angular.module(moduleName);
    var controllerName = "BooksController";
    var injectedDependencies = ['$scope', '$timeout', '$filter', '$rootScope', 'bookService','$state'];
    /* End: Controller Configuration */

    /* Start:Controller Definition */
    var controller = function ($scope, $timeout, $filter, $rootScope, bookService,$state) {


        /*Set Books in scope
        * */
        var setBooks = function (books) {
            $scope.books = books;
        };
        /*
         * Currently since we are fetching json locally so no error handling is done
         * */
        var showError = function (error) {
            $scope.error = error;
        };
        /*Hide parent view if any child route is open*/
        var showParentView=function(){
            if($state.current.name=='books'){
                return true;
            } else {
                return false;
            }
        };
        $scope.showParentView=showParentView;
        //Fetch Books
        bookService.getBooks(setBooks, showError);
    };
    /* End:Controller Definition */

    /* Register the controller */
    controller.$inject = injectedDependencies;
    module.controller(controllerName, controller);
}());
