/**
 * <pre>
 * API Type: Angular Module,
 * Description :
 * This is an angular Module used bootstrap the application by angular.
 * It has associated controller and service which deals with data management on the views
 * </pre>
 */
(function () {
    /* Start:Module Configuration */
    var moduleName = "bookModule";
    // Module Dependencies
    var injectedDependencies = ['ui.router','ionic'];
    /* End Configuration */
    var configFunction = function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state('books', {
            url: '/',
            views: {
                'contentView': {
                    templateUrl: 'books.html',
                    controller: 'BooksController'
                }
            }

        }).state('book', {
            url: '/book/:id',
            views: {
                'contentView': {
                    templateUrl: 'book.html',
                    controller: 'BookController'
                }
            }
        }).state('help', {
            url: '/help',
            views: {
                'contentView': {
                    templateUrl: 'help.html'
                }
            }
        });
        $urlRouterProvider.otherwise('/');
    }

    var config = ['$stateProvider', '$urlRouterProvider', configFunction];
    // Register Module
    angular.module(moduleName, injectedDependencies).config(config);
}());
