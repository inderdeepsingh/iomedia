/**
 * <pre>
 * API Type:Angular Service,
 * Module: bookModule,
 * Description :
 * This is API used for calling Rest API's to fetch books.
 * </pre>
 */
(function() {
	/* Start:Service Configuration */
	var moduleName = "bookModule";
	var module = angular.module(moduleName);
	var serviceName = "bookService";
	var injectedDependencies = [ '$http','$filter' ];
	/* End: Configuration */

	/* Start:Service Definition */
	var service = function($http,$filter) {
		var books=null;
		/**
		 * get Books from json file
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 * */
		var getBooks = function(successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					books=response.data;
					successCallback(response.data,response);
				}
			};
			var fail = function(response) {			
				if (failCallback) {
					failCallback(response.data,response);
				}
			}
			$http.get("books.json").then(success, fail);
		};
		/**
		 * get Book By Id. If books are already fetched then required book is returned else
		 * Books are fetched and this function is added as a successcallback to getBooks function
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 * */
		var getBookById=function(id,successCallback, failCallback){
			if(books){
				var book=$filter('filter')(books,{'id':parseInt(id)})[0];
				successCallback(book);
			} else {
				getBooks(function(){getBookById(id,successCallback)});
			}
		}
		return {
			"getBooks" : getBooks,
			"getBookById":getBookById
		};
	};
	/* End:Service Declaration */

	/* Start:Service Registration */
	service.$inject = injectedDependencies;
	module.factory(serviceName, service);
	/* End:Service Registration */
}());