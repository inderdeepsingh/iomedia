/**
 * <pre>
 * API Type: Angular Controller,
 * Module: bookModule,
 * Description :
 * This is API used as controller for controlling the index.html view.
 * </pre>
 */
(function () {
    /* Start: Controller Configuration */
    var moduleName = "bookModule";
    var module = angular.module(moduleName);
    var controllerName = "ApplicationController";
    var injectedDependencies = ['$scope', '$ionicPopup', '$state'];
    /* End: Controller Configuration */

    /* Start:Controller Definition */
    var controller = function ($scope, $ionicPopup,$state) {
        $scope.bookDetails={};
        /**
         * Open Book Details Popup
         * */
        var bookDetailsPopup = function () {
            $scope.bookDetails.bookId=null;
            $ionicPopup.show({
                template: '<input type="text" ng-model="bookDetails.bookId">',
                title: 'Enter Book Id',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'},
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            /*If book id is not null prevent event*/
                            if (!$scope.bookDetails.bookId) {
                                e.preventDefault();
                            } else {
                                $state.go('book', {id: $scope.bookDetails.bookId})
                            }
                        }
                    },
                ]
            });
        };
        $scope.bookDetailsPopup = bookDetailsPopup;

    };
    /* End:Controller Definition */

    /* Register the controller */
    controller.$inject = injectedDependencies;
    module.controller(controllerName, controller);
}());
